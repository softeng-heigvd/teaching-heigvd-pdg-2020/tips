# Deployment

_Please read the [Requirements](REQUIREMENTS.md) before continuing._

- [Deploy on a custom virtual machine (VM)](vm/README.md)
