# Configure GitLab CI

These instructions allow GitLab CI to connect to the VPN (if needed) and launch the environment(s) on the VM.

In most case, a VPN connection should not be required as most of the cloud platforms allows to directly connect through SSH to the server. This is required by the HEIG-VD for security reasons.

## Add the variables to GitLab CI

_Please be aware that any members that have at least the `Maintainer` role can see the GitLab variables and their content._

The following variables must be added to the [GitLab CI UI](https://docs.gitlab.com/ee/ci/variables/README.html#create-a-custom-variable-in-the-ui):

- `VPN_HOSTNAME`

  Use the hostname or IP required to connect to the VPN. To connect to the HEIG-VD VPN, use `remote.heig-vd.ch`.

- `VPN_USER_USERNAME`

  This is the username used to connect to the VPN. Use one of the team member's credentials for this step as we cannot create a specific VPN user for this project for the moment.

- `VPN_USER_PASSWORD`

  This is the password used to connect to the VPN. Use one of the team member's credentials for this step as we cannot create a specific VPN user for this project for the moment.

- `VM_HOSTNAME`

  Use the hostname or IP that was provided to connect to the VM.

- `VM_USER_USERNAME`

  Use the username that was provided to connect to the VM.

- `VM_USER_SSH_PRIVATE_KEY`

  The SSH private key has been created from the step _[Configure SSH authentication](CONFIGURE_SSH_AUTHENTICATION.md)_.

  _These steps are done from the local machine._

  ```sh
  # Display the private key
  cat ~/.ssh/nos-oiseaux-vm
  ```

  Copy and add the private key in GitLab CI Variables (don't forget to put an empty line at the end).

## Add the required steps to `.gitlab-ci.yml`

The following step example must be added to the `.gitlab-ci.yml` file in order to deploy the project through the GitLab CI pipeline.

```sh
deploy environment:
  # Define the job's stage
  stage: deployment
  # Define when the job should be run
  rules:
    - if: $CI_COMMIT_BRANCH == "master"
      when: on_success
  # Disable inheritage from global defaults and variables (optional)
  inherit:
    default: false
    variables: false
  # Define the image to use
  image: alpine:edge
  # Install all the required packages and connect to the VPN
  before_script:
    # Exit immediately if a command exits with a non-zero status
    - set -o errexit
    # Treat unset variables as an error when substituting
    - set -o nounset
    # Add required packages
    - apk add --no-cache openconnect openssh-client
    # Connect to the VPN
    - echo "$VPN_USER_PASSWORD" | openconnect --user=$VPN_USER_USERNAME --passwd-on-stdin --background $VPN_HOSTNAME
    # Hacky: wait for the VPN to be up
    - sleep 5
    # Configure SSH
    - mkdir -p ~/.ssh
    - chmod 700 ~/.ssh
    # Create the SSH private key
    - echo "$VM_USER_SSH_PRIVATE_KEY" > ~/.ssh/id_ed25519
    - chmod 700 ~/.ssh/id_ed25519
    # Add the VM hostname to known hosts
    - ssh-keyscan "$VM_HOSTNAME" > ~/.ssh/known_hosts
    - chmod 644 ~/.ssh/known_hosts
  script:
    # Execute the commands on the VM
    - ssh $VM_USER_USERNAME@$VM_HOSTNAME "cd nos-oiseaux-docker-files; git pull" # Get the latest version of the docker files
    - ssh $VM_USER_USERNAME@$VM_HOSTNAME "cd nos-oiseaux-docker-files; docker-compose pull" # Get the latest version of the docker images
    - ssh $VM_USER_USERNAME@$VM_HOSTNAME "cd nos-oiseaux-docker-files; docker-compose up --detach" # Start the infrastructure in background
```
