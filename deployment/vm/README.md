# Deploy on a custom virtual machine (VM)

1. [Configure SSH authentication](CONFIGURE_SSH_AUTHENTICATION.md)
1. [Install and configure Docker](INSTALL_AND_CONFIGURE_DOCKER.md)
1. [docker-compose files and configuration on the server](DOCKER_COMPOSE_FILES_AND_CONFIGURATION_ON_THE_SERVER.md)
1. [Configure GitLab CI](CONFIGURE_GITLAB_CI.md)
