# Configure SSH authentication

These steps allow to connect to a server though SSH without using a password.

## Configure the local machine

```sh
# Create the SSH directory on the local machine
mkdir -p ~/.ssh

# Restrict the SSH directory on the local machine to yourself
chmod 700 ~/.ssh

# Generate the public/private SSH keys
ssh-keygen -t ed25519 -f ~/.ssh/nos-oiseaux-vm -q -N ""
```

## Configure the server

```sh
# Connect to the server though SSH
ssh <user>@<host>

# Create the SSH directory on the server
mkdir -p ~/.ssh

# Restrict the SSH directory on the server to yourself
chmod 700 ~/.ssh

# From the local machine, copy the public key to the server
ssh-copy-id -i ~/.ssh/nos-oiseaux-vm.pub <user>@<host>
```

You should now be able to access the server from your local machine without any password.
