# Install and configure Docker

These steps allow to install Docker and Docker Compose on the server based on Ubuntu.

```sh
# Get the latest updates
sudo apt update

# Install the latest updates
sudo apt upgrade

# Add Docker APT repository
sudo apt install \
    apt-transport-https \
    ca-certificates \
    curl \
    gnupg-agent \
    software-properties-common

curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -

sudo add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
   $(lsb_release -cs) \
   stable"

# Install Docker
sudo apt-get install docker-ce docker-ce-cli containerd.io

# Install Docker Compose as a container
sudo curl -L --fail https://github.com/docker/compose/releases/download/1.27.4/run.sh -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose

# Enable the docker service on boot
sudo systemctl enable docker

# Add the current user to the group `docker`
sudo usermod -aG docker $USER
```

If you log out and log in again, you should be able to use Docker / Docker Compose as a regular user.
