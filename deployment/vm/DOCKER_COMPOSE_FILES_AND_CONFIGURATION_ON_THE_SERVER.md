# docker-compose files and configuration on the server

These steps allow to have the docker-compose files on the server and run the infrastructure.

_These steps are very dependent on how your project is structured and are not meant to the "the truth", the best solution. Depending on your needs, project structure and high availabilty requirements, this might not be the right solution at all. In the context of the PDG course, this is more than enough._

```sh
# Clone the repository containing the production docker-compose files
git clone https://gitlab.com/nos-oiseaux/nos-oiseaux-docker-files.git

# Move to the cloned directory
cd nos-oiseaux-docker-files

# Configure the required elements of your project (might be optional)
[...]

# Start the infrastructure for testing
docker-compose up
```
