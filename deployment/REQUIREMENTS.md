# Requirements

These prerequisites are only advice and some of you may be much more advanced than that. It is not mandatory to have these prerequisites for this course but it can give you an idea of where you need to go.

Most of your projects consists of:

- A frontend (The UI used to access your service - React, Vue.js, ...)
- A backend (Where the business logic happens - Spring Boot, Express.js, ...)
- A database (Where the data is stored - MySQL, PostgreSQL, MongoDB, ...)
- A website to present your project (for investors, clients, friends, interviews - Pure HTML/CSS/JS or a frontend framework)
- A reverse-proxy (Which redirects `app.my-cool-project.ch` to the frontend service, `api.my-cool-project.ch` to the backend service, `my-cool-project.ch` to the website service, ... Modern reverse-proxy manage TLS/SSL automatically as well. - Traefik, Caddy, ...)

Most of the time, when starting a project from scratch, the first step is to identify the technological stack (after identifying what we want to achieve with the mockups!). When the technological stack is identified, we try to create a proof of concept (PoC) / a minimum valuable product (MVP) to ensure that the technological stack can work together. Think of it as a "hello world" project. We do not care much about business features for the moment.

The second step is to create a pipeline to ensure the PoC / MVP can be built, tested, released and deployed in the _most_ automated way possible. It is not required to automate the 100% of the pipeline. If only the restarting of the infrastructure has to be done by hand on the server, it is fine. This can be a tiring and frustrated step. But when it works, it's true happiness that comes out (have you ever made a complete pipeline to release your software ? no ? you haven't experimented true happiness then).

Not only this pipeline gives you happiness but you can now concentrate on the business logic and development of new features: no more pipeline to think about for the rest of the project, yeah !

So here are some tips/a guideline to acheive the pipeline.

## You have been able to make a PoC / MVP of your technological stack.

You are able to make it work locally (which gives you the "But it works on my machine" badge!) but you need to make it work in Docker.

On your machine, you used `localhost:3306` to access your MySQL instance but in Docker, you cannot use `localhost`; you should use the database service's name, such as `db:3306` for example.

This means you have to dynamically change the value of the `DATABASE_URL` in your code, depending or your environment (the local environment or the Docker environment).

Many solutions exist but the easiest one (in my opinion) is to use [Environment variables](https://en.wikipedia.org/wiki/Environment_variable).

You can then set the value `localhost:3306` to access the database locally and the value `db:3306` for the Docker environment ([Documentation](https://docs.docker.com/compose/environment-variables/)).

Usually, environment variables are set using `export` (on *nix systems) or adding them to through the Windows' settings.

Thankfully, libs exist to "inject" environment variables in the process by making usage of the `.env` / `.env.example` files:

- `.env` contains the environment variables and their values currently used by the process. This file is private and should not be stored in git.
- `.env.example` the environment variables with fake values. This is public and can be stored in git.

Many libs (usually known as `dotenv`) make usage of the file `.env` and even Docker Compose can use it directly out-of-the-box to populate the environment variables.

This can be an elegant solution to use environment variables in your project and switch between your local environment and the Docker one.

Here is a list of some of these libs:

- https://www.npmjs.com/package/dotenv (Node.js)
- https://github.com/paulschwarz/spring-dotenv (Spring Boot)
- https://docs.spring.io/spring-boot/docs/1.1.x/reference/html/boot-features-profiles.html (Spring Boot)

## You are now able to switch between environments easily.

You have earned the "I am the new Two-Face. Batman, here I come." badge. You now need to make usage of Docker to "dockerize" your backend, frontend and website.

You have to create a Dockerfile for your frontend and another Dockerfile for your backend. These files will allow to build and pack your services as Docker images that you will be able to use later. So there will be two Docker images "hand made" by you for the frontend and the backend, two Docker image sfrom Docker Hub for your database and the reverse-proxy. Do not put the all the services in a single Docker image. You will then use Docker Compose to use those services.

Make usage of the environment variables as described earlier to set the correct values to access the database and more !

## You can now create Docker images locally

(sorry, no badges here).

When you create a Docker image (using `docker build`), it is only available on your machine. It is then necessary to make this Docker image available to other developers using a "Container Registry". Docker Hub is a container registry. GitLab also provides a Container Registry on which you will be able to make your Docker images available.

The upload to the GitLab Container Registry can be done locally but you will be using GitLab CI to [publish the Docker images during the CI/CD pipeline](https://docs.gitlab.com/ee/user/packages/container_registry/#container-registry-examples-with-gitlab-cicd). This should be done only when new features are merged on `master` or when a tag is created, for example.

## You have now Docker images for the rest of the team and for deployment !

Here is the "I like P2P technologies" badge.

The images are then available for the rest of the team as well as for deployment. Some new docker-compose files can be created to make usage of the images available on the GitLab Container Registry or you can use the [`override` functionality from Docker](https://docs.docker.com/compose/extends/). These docker-compose files should be run on the VM at the end.

## Deploy the Docker infrastructure on the server

Once the images are created and made available on the container registry, we can deploy the infrastructure on the server.

Connect to the VM through SSH, clone the repository containing the production docker-compose files. Do a `docker-compose up` to launch our infrastructure with the images from the container registry. There will be nothing to build on the VM, just get the latest version of the Docker images (with a `docker-compose pull`) and restart the infra (`docker-compose-up`) and we are good.

You have earned the "DevOps 101" badge, congrats !

So to sum up:

- Manage the different environments correctly
- Create Dockerfiles for your services
- Make these Docker images available
- Use these images Docker

The next steps should help you to configure the server and the GitLab CI pipeline to deploy your app.
